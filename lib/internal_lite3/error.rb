

module Lite3
  # Exception class for errors specific `Lite3::DBM`.
  class Error < StandardError; end

  # Exception class for internal errors; if you get one of these,
  # either my code or your code is incorrect.
  class InternalError < Error; end

  private

  module ErrUtil
    # Error check: if block evaluates to false, raise a Lite3::DBM::Error
    # with the given message.
    def check(message, &block)
      return if block && block.call
      raise InternalError.new(message)
    end
  end

  private_constant :ErrUtil
end
