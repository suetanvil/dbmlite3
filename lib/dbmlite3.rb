
require 'sequel'
require 'psych'
require 'set'
require 'yaml'

module Lite3
  IS_JRUBY = (RUBY_PLATFORM == "java")

  private_constant :IS_JRUBY
end


require_relative 'internal_lite3/error.rb'
require_relative 'internal_lite3/handle.rb'
require_relative 'internal_lite3/sql.rb'
require_relative 'internal_lite3/dbm.rb'

