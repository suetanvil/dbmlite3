
JRUBY_GEM = (ENV['JRUBY_GEM'] == "yes")

if JRUBY_GEM
  SUFFIX = '_jruby'
  DESC_EXTRA = <<EOF

    This is the JRuby version of dbmlite3; it is identical to the MRI
    version except for its dependencies.
EOF
  SUMMARY_EXTRA = " (JRuby version)"
else
  SUFFIX = ''
  DESC_EXTRA = ''
  SUMMARY_EXTRA = ''
end


Gem::Specification.new do |s|
  s.name        = "dbmlite3#{SUFFIX}"
  s.version     = '2.0.1'
  s.date        = '2024-03-30'
  s.summary     = "A DBM-style key-value store using SQLite3#{SUMMARY_EXTRA}"
  s.description = <<-EOF + DESC_EXTRA
    Lite3::DBM is an object that behaves like a Ruby Hash but stores
    its data in a SQLite3 database table.  It is a drop-in replacement
    for DBM.

    Values are optionally serialized with YAML or Marshal, allowing
    (almost) any Ruby type to be stored.  In addition, there is
    rudimentary support for transactions with emphasis on simplicity
    and safety.
EOF
  s.authors     = ["Chris Reuter"]
  s.email       = 'chris@remove-this-part.blit.ca'

  # I'm just going to add everything so that if you've got the gem,
  # you've also got the source distribution.  Yay! Open source!
  s.files       =
    Dir.glob('doc/**/*') +
    `git ls-files`.split.reject {|f| f =~ /\.org$/}


  s.required_ruby_version = '>= 2.7.0'
  s.requirements << (JRUBY_GEM                          ?
                     "Sequel, jdbc-sqlite3, JRuby"  :
                     "Sequel, sqlite3, Ruby MRI")

  s.add_runtime_dependency "sequel", '~> 5.71.0'

  if JRUBY_GEM
    s.add_runtime_dependency "jdbc-sqlite3", "~> 3.42.0.0"
  else
    s.add_runtime_dependency "sqlite3", "~> 1.6.1"
  end

  s.add_development_dependency "rspec", '~> 3.10', '>= 3.10.0'
  s.add_development_dependency "yard", '~> 0.9.25', '>= 0.9.25'

  s.homepage    = 'https://codeberg.org/suetanvil/dbmlite3'
  s.license     = 'MIT'
end
